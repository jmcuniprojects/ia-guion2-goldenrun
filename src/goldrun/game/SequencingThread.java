/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Class SequencingThread moves all DwarfInstances in a round robin manner. This is 
 * executed as a Thread.
 */
public class SequencingThread
	extends Thread
{

	private ArrayList<DwarfRepresent> dwarfInstances;
	private HashMap<DwarfRepresent, Integer> diseaseSymptomsCounter;
	private volatile boolean isAlive;

	/**
	 * Creates a new Instance of the SequencingThread
	 */
	public SequencingThread()
	{
		dwarfInstances = new ArrayList<DwarfRepresent>();
		diseaseSymptomsCounter = new HashMap<DwarfRepresent, Integer>();
	}
	
	/**
	 * Adds a new DwarfRepresent that will be moved by the SequencingThread
	 * @param instance The DwarfRepresent instance to be added to the sequence 
	 */
	public void addInstance(DwarfRepresent instance)
	{
		dwarfInstances.add(instance);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		isAlive = true;
		
		HashMap<DwarfRepresent, Grid> movableDwarf = new HashMap<DwarfRepresent, Grid>();
		HashMap<Long, ArrayList<DwarfRepresent>> dwarfTime = new HashMap<Long, ArrayList<DwarfRepresent>>(); 
		HashMap<DwarfRepresent, Integer> dwarfMove = new HashMap<DwarfRepresent, Integer>();
		ArrayList<Long> times = new ArrayList<Long>();
		
		while (isAlive)
		{
			movableDwarf.clear();
			dwarfTime.clear();
			dwarfMove.clear();
			times.clear();
			
			ArrayList<DwarfRepresent> retireList = new ArrayList<DwarfRepresent>();
			for (DwarfRepresent instance : dwarfInstances)
			{
				if (determineDwarfRetire(instance))
				{
					retireList.add(instance);
				}
			}
			
			for (DwarfRepresent instance : retireList)
			{
				instance.retire();
				dwarfInstances.remove(instance);
				Debug.out().println("FATAL: " + instance.getName() + " ha sido obligado a retirarse.");
			}
			
			for (DwarfRepresent instance : dwarfInstances)
			{
				try
				{
					Grid grid = instance.control();
					if (grid != null)
					{
						movableDwarf.put(instance, grid);
					}
				}
				catch (IOException ioex)
				{
	
				}
			}
			
			for (DwarfRepresent instance : movableDwarf.keySet())
			{
				final DwarfRepresent instanceF = instance;
				final Grid gridF = movableDwarf.get(instance);

				ExecutorService exe = Executors.newCachedThreadPool();
				Callable<Integer> caller = new Callable<Integer>() 
				{
					public Integer call()
					{
						return instanceF.getNextMove(gridF);
					}
				};
				
				Future<Integer> future = exe.submit(caller);
			
				try
				{
					long start = System.nanoTime();
					int move = future.get(GameConfig.DWARF_RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS);
					long end = System.nanoTime();
					
					long time = end - start;
					
					dwarfMove.put(instance, move);
					
					if (dwarfTime.containsKey(time))
					{
						dwarfTime.get(time).add(instance);
					}
					else
					{
						ArrayList<DwarfRepresent> rep = new ArrayList<DwarfRepresent>();
						rep.add(instance);
						dwarfTime.put(time, rep);
					}
					
					times.add(time);
					resetDwarfSymptoms(instance);
				}
				catch (TimeoutException toe)
				{
					incrementSymptomCount(instance);
					Debug.out().println("ADVERTENCIA: " + instance.getName() + " es muy lento. Aviso #" + getNumberOfSymptoms(instance));
				}
				catch (Exception ee)
				{
					if (ee.getCause() != null && ee.getCause() instanceof SecurityException)
					{
						retireImmediate(instance);
						Debug.out().println("FATAL: " + instance.getName() + " no est\u00E1 respetando las reglas. Ser\u00E1 descalificado. ");					
					}
					else
					{
						incrementSymptomCount(instance);
						Debug.out().println("ADVERTENCIA: " + instance.getName() + " ha lanzado una excepci\u00F3. Aviso #" + getNumberOfSymptoms(instance));
						Debug.out().println("Excepci\u00F3n: " + ee.getMessage());
						//ee.printStackTrace();
					}
				}
				finally
				{
					future.cancel(true);
				}
			}
			
			while (times.size() > 0)
			{
				long time = Collections.min(times);
				times.remove(time);
				
				ArrayList<DwarfRepresent> repList = dwarfTime.get(time);
				Random random = new Random();
				while (repList.size() > 0)
				{
					DwarfRepresent rep = repList.get(random.nextInt(repList.size()));
					rep.moveDwarf(dwarfMove.get(rep));
					repList.remove(rep);
				}
			}
					
			try	{ Thread.sleep(GameConfig.ROUND_SLEEP_TIME); } catch (InterruptedException itex) {	}
		}
	}
	
	/**
	 * Gets all DwarfRepresents that are being moved in sequence by the SequencingThread
	 * @return All DwarfRepresents that are being moved in sequence by the SequencingThread
	 */
	public ArrayList<DwarfRepresent> getInstances()
	{
		return dwarfInstances;
	}
	
	/**
	 * Stops the Thread
	 */
	public void kill()
	{
		isAlive = false;
	}
	
	// Increases the symptom count for the DwarfRepresent by 1
	private void incrementSymptomCount(DwarfRepresent instance)
	{
		if (!diseaseSymptomsCounter.containsKey(instance))
		{
			diseaseSymptomsCounter.put(instance, 1);
		}
		else
		{
			int counter = diseaseSymptomsCounter.get(instance);
			counter++;
			diseaseSymptomsCounter.put(instance, counter);
		}
	}
	
	// Mark the Dwarf to be immediately retire.
	private void retireImmediate(DwarfRepresent instance)
	{
		diseaseSymptomsCounter.put(instance, GameConfig.DISEASES_TO_RETIRE);
	}
	
	// Resets the DwarfRepresent symptoms to 0
	private void resetDwarfSymptoms(DwarfRepresent instance)
	{
		diseaseSymptomsCounter.put(instance, 0);
	}
	
	// Determines if the Dwarf is giving too much problem and returns a boolean value indicating whether mouse should retire.
	private boolean determineDwarfRetire(DwarfRepresent instance)
	{
		if (!diseaseSymptomsCounter.containsKey(instance))
		{
			return false;
		}
		else
		{
			int counter = diseaseSymptomsCounter.get(instance);
			return counter >= GameConfig.DISEASES_TO_RETIRE;
		}
	}
	
	private int getNumberOfSymptoms(DwarfRepresent instance)
	{
		if (!diseaseSymptomsCounter.containsKey(instance))
		{
			return 0;
		}
		else
		{
			int counter = diseaseSymptomsCounter.get(instance);
			return counter;
		}
	}

}
