/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

import goldrun.game.common.ImagedPanel;
import java.io.*;
import javax.swing.*;
import java.text.DecimalFormat;

/**
 * Class DwarfRepresent is the link between the DwarfController and the Dwarf
 * graphical interface. It is also responsible for moving the dwarf on each turn
 * towards the target grid of the dwarf and display the correct asset image to
 * represent the moving direction.
 */
public class DwarfRepresent
{
	private GameController controller;
	private DwarfController dwarf;
	private ImagedPanel represent;
	private JLabel nameLabel;
	private JLabel goldLabel;
	private int lastDirection;
	private String leftAsset;
	private String rightAsset;
	private String downAsset;
	private String upAsset;
	private String name;
	private DecimalFormat format;
	private int goldDisplayDuration;

	/**
	 * Creates a new instance of the DwarfRepresent
	 * @param controller The game controller, hosting the game.
	 * @param dwarf The DwarfController that will be represented by the DwarfRepresent
	 * @param represent The ImagedPanel that will display the Dwarf on the game interface
	 * @param nameLabel The Label that will display the Dwarf name on the game interface
	 * @param upAsset The asset that display the Dwarf going upward
	 * @param downAsset The asset that display the Dwarf going downward
	 * @param leftAsset The asset that display the Dwarf going left
	 * @param rightAsset The asset that display the Dwarf going right
	 */
	public DwarfRepresent(GameController controller, DwarfController dwarf, ImagedPanel represent, JLabel nameLabel, JLabel goldLabel,
		String upAsset, String downAsset, String leftAsset, String rightAsset)
	{
		this.controller = controller;
		this.dwarf = dwarf;
		this.represent = represent;
		this.nameLabel = nameLabel;
		this.goldLabel = goldLabel;
		this.format = new DecimalFormat(GameConfig.GOLD_NUMBER_FORMAT);
		this.goldDisplayDuration = 0;
		
		String name = dwarf.getDwarf().getName();
		if (name.length() > 13)
		{
			name = name.substring(0, 10) + "...";
		}
		
		this.name = name;
		
		this.nameLabel.setText("(" + dwarf.getNumberOfGold() + ") " + name);
		this.nameLabel.setSize(this.nameLabel.getPreferredSize());

		this.upAsset = upAsset;
		this.downAsset = downAsset;
		this.leftAsset = leftAsset;
		this.rightAsset = rightAsset;
		
		this.lastDirection = 0;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * This method is responsible for moving the dwarf towards it target grid. It will
	 * invoke the GameController to inform of its movement.
	 * @return True if dwarf is ready to receive a new instruction. False if otherwise
	 * @throws IOException An IOException can occur if the assets required are missing
	 */
	public Grid control()
		throws IOException
	{
		int direction = 0;
		int targetX = controller.getGridLeft(dwarf.getTargetGrid().getX());
		int targetY = controller.getGridTop(dwarf.getTargetGrid().getY());
		
		int x = represent.getX();
		int y = represent.getY();
		
		int leeway = GameConfig.PIXELS_ON_TARGET_LEEWAY;
		
		if (x > targetX + leeway)
		{
			x -= dwarf.getSpeed();
			direction = 1;
		}
		
		if (x < targetX - leeway)
		{
			x += dwarf.getSpeed();
			direction = 2;
		}
		
		if (y > targetY + leeway)
		{
			y -= dwarf.getSpeed();
			direction = 3;
		}
		
		if (y < targetY - leeway)
		{
			y += dwarf.getSpeed();
			direction = 4;
		}
		
		if (lastDirection != direction)
		{
			lastDirection = direction;
			
			switch (direction)
			{
				case 1:
					represent.setImage(this.leftAsset);
					break;
					
				case 2:
					represent.setImage(this.rightAsset);
					break;
					
				case 3:
					represent.setImage(this.upAsset);
					break;
				
				case 4:
					represent.setImage(this.downAsset);
					break;
			}
		}
						
		represent.setLocation(x, y);
		nameLabel.setLocation(x, y + represent.getHeight());
		goldLabel.setLocation(x + (represent.getWidth() / 4), y - 20);
		nameLabel.setText(name);
		
		if (goldDisplayDuration > 0)
		{
			goldLabel.setText(format.format(dwarf.getNumberOfGold()));
			goldDisplayDuration--;
		}
		else
		{
			goldLabel.setText("");
		}
		
		return controller.report(this, x, y);
	}
	
	/**
	*	Gets the Dwarf's next move
	*	@param targetGrid The grid the Dwarf is currently at
	*	@return The Dwarf move decision
	*/
	public int getNextMove(Grid targetGrid)
	{
		return controller.getDwarfNextMove(dwarf, targetGrid);
	}
	
	/**
	*	Causes the dwarf to move
	*	@param move The move decision made by the Dwarf.
	*/
	public void moveDwarf(int move)
	{
		controller.causeDwarfMove(dwarf, move);
	}
	
	public void displayGoldNumber()
	{
		goldDisplayDuration = GameConfig.DWARF_GOLD_DISPLAY_LENGTH;
	}
	
	/**
	 * Gets the JPanel that represents the Dwarf.
	 * @return The JPanel that represents the Dwarf.
	 */
	public JPanel getRepresent()
	{
		return this.represent;
	}
	
	/**
	 * Gets The DwarfController whose Dwarf is represented by DwarfRepresent
	 * @return The DwarfController represented by this instance.
	 */
	public DwarfController getDwarfController()
	{
		return this.dwarf;
	}
	
	/**
	*	Causes the Dwarf to display that is has retired.
	*/
	public void retire()
	{
		this.nameLabel.setText(getName() + " ha sido retirado.");
		goldLabel.setText("XXX");
	}

}