/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

/**
 * Class Gold is the object that the Dwarf implementations have to race to consume.
 * One gold will appear on the Maze at any one time and the location of the gold
 * will be known to all Dwarf implementations. The number of gold to play for will
 * be defined at the start of the game. The player whose Dwarf implementation consume
 * the most gold at the end of the game wins.
 */
public class Gold
{
	private int x;
	private int y;
	
	/**
	 * Creates an instance of the gold.
	 * @param x The X-axis of the gold in the maze.
	 * @param y The Y-axis of the gold in the maze.
	 */
	public Gold(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Get the X-axis of the gold in the maze.
	 * @return The X-axis of the gold in the maze.
	 */
	public int getX()
	{
		return this.x;
	}
	
	/**
	 * Get the Y-axis of the gold in the maze.
	 * @return The Y-axis of the gold in the maze.
	 */
	public int getY()
	{
		return this.y;
	}

}