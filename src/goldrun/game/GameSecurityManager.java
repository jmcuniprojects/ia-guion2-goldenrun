/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

import java.util.*;
import java.io.FileDescriptor;
import java.net.InetAddress;
import java.security.Permission;
import goldrun.dwarf.DwarfLoader;

/*
* Class GameSecurityManager secures the game by preventing Dwarf, which are implemented by third party developers, 
* from performing obscure but potentially dangerous activities.
*/
public final class GameSecurityManager
	extends SecurityManager
{
	private static GameSecurityManager current = null;
	
	// Creates a new instance of GameSecurityManager
	private GameSecurityManager()	{ }
	
	@Override 
	public void checkPermission(Permission perm)
	{
		inspectPermission(perm.getName());
	}
	
	@Override
	public void checkExit(int status)
	{
		inspectPermission("exitVM");
	}
	
	private void inspectPermission(String name)
	{
		switch (name)
		{
			case "setIO":
			case "suppressAccessChecks":
			case "createClassLoader":
			case "getClassLoader":
			case "setSecurityManager":
			case "exitVM":
			case "shutdownHooks":
			case "modifyThread":
			case "stopThread":
			case "accessDeclaredMembers":			
				denyDwarf();
				break;
		}
	}
	
	// This method is to throw a SecurityException if a Dwarf is in anyway
	// involve in the current stack.
	private void denyDwarf()
	{
		if (isTriggeredByDwarf())
		{
			throw new SecurityException();
		}
	}

	// This method determines if the Dwarf is in anyway involved in the stack.
	private boolean isTriggeredByDwarf()
	{
		StackTraceElement[] elements = new Throwable().getStackTrace();
		for (Class<?> dwarfClass : DwarfLoader.getDetectedDwarfClasses())
		{
			for (StackTraceElement element : elements)
			{
				if (element.getClassName().equals(dwarfClass.getCanonicalName()))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	/*
	* Installs the GameSecurityManager to this game.
	*/
	public static GameSecurityManager install()
	{
		if (current == null)
		{
			current = new GameSecurityManager();
			if (GameConfig.INSTALL_SECURITY_MANAGER)
			{
				System.setSecurityManager(current);
			}
		}
		
		return current;
	}
		
}