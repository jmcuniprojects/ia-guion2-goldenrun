/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

/**
 * Abstract Class Dwarf should be the base class of all Dwarf implementations. Players
 * implementing the Dwarf must implement all abstract methods and place the dwarf in 
 * the dwarfrun.dwarf package. DwarfLoader will locate all dwarf implementation in the
 * package.
 */
public abstract class Dwarf
{

	public static final int UP 		=	1;
	public static final int DOWN		=	2;
	public static final int LEFT		=	3;
	public static final int RIGHT		=	4;
	public static final int BOMB       = 	5;
	
	private String name;
        
        // New: 01-21-2016
        // New private field, different grids visited by the dwarf
        private long exploredGrids;
        
        // New: 01-21-2016
        // New private field, total grids visited by the dwarf
        private long steps;
	
	/**
	 * Creates a new instance of Dwarf.
	 * @param name The name of the Dwarf to appear in the game interface.
	 */
	public Dwarf(String name)
	{
		this.name = name;
                this.exploredGrids = 0; // New: 01-21-2016
                this.steps = 0; // New: 01-21-2016
	}
	
	/**
	 * Get the name of the dwarf that appears on the game interface.
	 * @return The name of the dwarf.
	 */
	public String getName()
	{
		return this.name;
	}
	
        // New: 01-21-2016
        /**
         * Get the current number of unique visited grids.
         * It must be managed by the user himself
         * @return number of unique visited grids
         */
        public long getExploredGrids()
        {
            return this.exploredGrids;
        }
        
        // New: 01-21-2016
        /**
         * Increment the current number of unique visited grids.
         * Use it fairly
         * @return updated number of unique visited grids
         */
        public long incExploredGrids()
        {
            return this.exploredGrids++;
        }
        
        // New: 01-21-2016
        /**
         * Get the current number of visited grids (steps).
         * It must be managed by the user himself
         * @return total number of visited grids (steps)
         */
        public long getSteps()
        {
            return this.steps;
        }
        
        // New: 01-21-2016
        /**
         * Increment the steps (visited grids) given by the dwarf.
         * Do not call this method, it is automatically managed by the application
         * @return updated number of visited grids (steps)
         */
        public long incSteps()
        {
            return this.steps++;
        }
        
	/**
	 * The move method is called every time a dwarf reaches a new grid. This method
	 * expects a decision to be made and returns the decision to be made on how to 
	 * move the Dwarf next. Use the Dwarf.UP, Dwarf.DOWN, Dwarf.LEFT, Dwarf.RIGHT,
	 * Dwarf.BOMB as options for the decision. Players can do that is need to implement
	 * an intelligent dwarf. 
	 * @param currentGrid The current grid the Dwarf is at.
	 * @param gold The gold to be seek.
	 * @return The decision to move the dwarf. Use Dwarf.UP, Dwarf.DOWN, Dwarf.LEFT, Dwarf.RIGHT, Dwarf.BOMB 
	 */
	public abstract int move(Grid currentGrid, Gold gold);
	
	/**
	 * This method is called every time a dwarf (including own implementation) consumes
	 * a cheese and a new cheese is relocated. Usually utilized for clean ups.
	 */
	public abstract void newGold();
	
	/**
	 * This method is called every time the Dwarf touches a bomb that is not planted by it.
	 * Usually utilized for calibration.
	 */
	public abstract void respawned();
	
}
