/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

/**
 * Class DwarfController is the wrapper class for each Dwarf implementation. This class
 * layers the connection between the GameController and the Dwarf itself to prevent
 * access to crucial information and manipulation.
 */
public class DwarfController
{
	private Dwarf dwarf;
	private int speed;
	private int numberOfGold;
	private Grid targetGrid;
	private int numberOfBombs;
	
	/**
	 * Creates a new instance of DwarfController
	 * @param dwarf The dwarf to be wrapped in the controller.
	 */
	public DwarfController(Dwarf dwarf)
	{
		this.dwarf = dwarf;
		this.speed = GameConfig.PIXELS_PER_TURN;
		this.numberOfGold = 0;
		this.numberOfBombs = 0;
	}

	/**
	 * This method is called each time the dwarf reaches a grid. This bridge to the
	 * move method implemented in the dwarf.
	 * @param currentGrid The current grid the Dwarf is at.
	 * @param gold The gold to seek.
	 * @return The move decision
 Modified on 01-21-2016 to consider new dwarf variable, steps
	 */
	public int onGrid(Grid currentGrid, Gold gold)
	{
                // New: 01-21-2016: Steps incremented automatically
                dwarf.incSteps();
		return dwarf.move(currentGrid, gold);
	}
	
	/**
	 * Sets the next grid the dwarf is heading to.
	 * @param targetGrid The next grid the dwarf is heading to.
	 */
	public void setTargetGrid(Grid targetGrid)
	{
		this.targetGrid = targetGrid;
	}
	
	/**
	 * Gets the next grid the dwarf is to head to.
	 * @return The next grid the dwarf is to head to.
	 */
	public Grid getTargetGrid()
	{
		return this.targetGrid;
	}
		
	/**
	 * Get the Dwarf wrapped in this DwarfController.
	 * @return Returns the Dwarf wrapped in this DwarfController
	 */
	public Dwarf getDwarf() 
	{
		return this.dwarf;
	}
	
	/**
	 * Get the number of pixels to move per dwarf move turn. 
	 * @return The number of pixels to move per dwarf move turn.
	 */
	public int getSpeed()
	{
		return this.speed;
	}
	
	/**
	 * Get the number of gold the current Dwarf has consumed thus far.
	 * @return The number of gold the current Dwarf has consumed.
	 */
	public int getNumberOfGold()
	{
		return this.numberOfGold;
	}
	
	/**
	 * Increases the number of gold consumed by the Dwarf.
	 */
	public void increaseNumberOfGold()
	{
		this.numberOfGold++;
	}
	
	/**
	 * Gets the number of Bombs the Dwarf still has.
	 * @return The number of Bombs available.
	 */
	public int getNumberOfBombs()
	{
		return this.numberOfBombs;
	}
	
	/**
	 * Sets the number of Bombs that Dwarf will have.
	 * @param numberOfBombs The number of Bombs the Dwarf will have.
	 */
	public void setNumberOfBombs(int numberOfBombs)
	{
		this.numberOfBombs = numberOfBombs;
	}
	
	/**
	 * Creates a new bomb for the Dwarf to be planted on the current targetGrid.
	 * If no Bomb is available, null will be returned.
	 * @return The Bomb to be planted by the Dwarf. If not available, null will be returned.
	 */
	public Bomb makeBomb()
	{
		if (numberOfBombs > 0)
		{
			numberOfBombs--;
			Bomb bomb = new Bomb(targetGrid.getX(), targetGrid.getY(), this.dwarf);
			return bomb;
		}
		
		return null;
	}
	
}
