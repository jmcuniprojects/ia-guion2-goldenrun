/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

/**
* Class GameConfiguration is a common location to edit Fixed game settings.
*/
public class GameConfig
{

	/** UI **/
	public static final String GAME_TITLE				= "Gold Run";
	public static final int GRID_LENGTH					= 30;
	public static final int DWARF_GOLD_DISPLAY_LENGTH	= 40;
	public static final int COUNT_DOWN_FONT_SIZE			= 200;
	public static final String GOLD_NUMBER_FORMAT		= "00";
	
	/** Assets **/
	public static final String ASSETS_BOMB				= "assets/bomb.png";
	public static final String ASSETS_EXPLODED			= "assets/exploded.png";
	public static final String ASSETS_GOLD				= "assets/gold.png";
	public static final String ASSETS_DWARFUP			= "assets/dwarfup.png";
	public static final String ASSETS_DWARFDOWN			= "assets/dwarfdown.png";
	public static final String ASSETS_DWARFLEFT			= "assets/dwarfleft.png";
	public static final String ASSETS_DWARFRIGHT			= "assets/dwarfright.png";
	
	/** Logic **/
	public static final double RATIO_BOMBS_TO_GOLD		= 0.1;
	public static final double RATIO_CLOSED_WALL_TO_OPEN	= 0.3;
	public static final int PIXELS_PER_TURN				= 10;
	public static final int PIXELS_ON_TARGET_LEEWAY		= 5;
	public static final int DISEASES_TO_RETIRE			= 5;
	public static final int DWARF_RESPONSE_TIMEOUT		= 2147483647;
	public static final int ROUND_SLEEP_TIME				= 80;

	/** Security **/
	public static final boolean INSTALL_SECURITY_MANAGER	= true;
	public static final boolean PREVENT_DWARF_IO			= true;
}