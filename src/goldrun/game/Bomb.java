/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
 package goldrun.game;

/**
 * Class Bomb. Any Dwarf implementation can drop a Bomb to hinder another Dwarf
 * from advancing. If a Dwarf comes across a Bomb that does not belong to it,
 * it will explode and be "respawned" at another random location. To drop a Bomb,
 * Dwarf should return Dwarf.BOMB in the move() method.
 * 
 * Players should be wary with the use of the Bomb because while it may hinder the
 * other Dwarf from advancing, it may relocate the other Dwarf closer to the Cheese.
 */
public class Bomb
{

	private int x;
	private int y;
	private boolean detonated;
	private Dwarf dwarf;
	
	/**
	 * Constructor. 
	 * Creates a new instance of the bomb.
	 * @param x The X axis of the location of the bomb.
	 * @param y The Y axis of the location of the bomb.
	 * @param dwarf The dwarf that planted the bomb.
	 */
	public Bomb(int x, int y, Dwarf dwarf)
	{
		this.x = x;
		this.y = y;
		this.dwarf = dwarf;
		detonated = false;
	}
	
	/**
	 * Get the X-axis of the location of the bomb.
	 * @return The X-axis of the location of the bomb.
	 */
	public int getX()
	{
		return x;
	}
	
	/**
	 * Get the Y-axis of the location of the bomb.
	 * @return The Y-axis of the location of the bomb.
	 */
	public int getY()
	{
		return y;
	}
		
	/**
	 * Get the Dwarf that planted the bomb.
	 * @return The Dwarf that planted the bomb.
	 */
	public Dwarf getDwarf()
	{
		return dwarf;
	}
	
	/**
	 * Determine if the bomb has been detonated when a dwarf comes across it.
	 * @return True if bomb has been detonated. False if otherwise.
	 */
	public boolean hasDetonated()
	{
		return detonated;
	}
	
	/**
	 * Mark the bomb has detonated if it has not been detonated already.
	 */
	public void detonate()
	{
		detonated = true;
	}

}