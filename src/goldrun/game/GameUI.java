/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.game;

import goldrun.game.common.ImagedPanel;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

/**
 * Class GameUI is the Game Interface of the game. It uses standard JFrame 
 * etc components in this implementation.
 */
public class GameUI
	extends JFrame
	implements GameControllerAdapter
{
	private Maze maze;
	private GameController controller;
	private int GRID_LENGTH = 30;
	private ImagedPanel[][] mazePanels;
	private JLayeredPane container;
	private JPanel goldPanel;
	private JLabel countDownLabel;
	private SequencingThread sequencer;
	private CountDownThread countDownThread;
	private ArrayList<BombRepresent> bombs;
	private int duration;
	
	/**
	 * Creates an instance of the GameUI.
	 * @param width The width of the user interface.
	 * @param height The height of the user interface.
	 * @param numberOfGold The number of cheese this game is playing for.
	 * @throws IOException An IOException can occur when the required game assets are missing.
	 */
	public GameUI(int width, int height, int numberOfGold, int duration)
		throws IOException
	{
		super(GameConfig.GAME_TITLE);
		GRID_LENGTH = GameConfig.GRID_LENGTH;
		
		this.controller = new GameController(this, width, height, GRID_LENGTH, numberOfGold);
		this.mazePanels = new ImagedPanel[width][height];
		this.maze = this.controller.getMaze();
		this.bombs = new ArrayList<BombRepresent>();
		this.sequencer = new SequencingThread();
				
		initialiseUI();
		controller.start();
		
		if (duration > 0)
		{
			countDownThread = new CountDownThread(this, duration);
			countDownThread.start();
		}
	}
	
	
	// Loads and defines the frame of the user interface, the maze, the mouse
	// and the objects.
	// @throws IOException An IOException can occur if the required game assets are missing.
	private void initialiseUI()
		throws IOException
	{
		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.pack();
		
		Insets insets = frame.getInsets();
		container = new JLayeredPane();
		container.setSize(new Dimension((maze.getWidth() * GRID_LENGTH), (maze.getHeight() * GRID_LENGTH)));
	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(Color.BLACK);
		this.setSize((maze.getWidth() * GRID_LENGTH) + insets.left + insets.right , (maze.getHeight() * GRID_LENGTH) + insets.top + insets.bottom);
		this.setLayout(null);
		this.add(container);
		this.setResizable(false);
		
		countDownLabel = new JLabel("");
		countDownLabel.setForeground(Color.WHITE);
		countDownLabel.setFont(new Font("San Serif", Font.PLAIN, GameConfig.COUNT_DOWN_FONT_SIZE));
		container.add(countDownLabel);
		
		for (int x = 0; x < maze.getWidth(); x++)
		{
			for (int y = 0; y < maze.getHeight(); y++)
			{
				Grid grid = maze.getGrid(x, y);
				String assetAddress = "assets/" + grid.getAssetName();
			
				ImagedPanel panel = new ImagedPanel(assetAddress, GRID_LENGTH, GRID_LENGTH);
				mazePanels[x][y] = panel;
			
				panel.setBounds(getGridLeft(x), getGridTop(y), GRID_LENGTH, GRID_LENGTH);
				container.add(panel);
			}
		}
		
		createGold();
	}
	
	// Creates a panel on the user interface representing a cheese.
	// @throws IOException An IOException can occur if the required game assets are missing.
	private void createGold()
		throws IOException
	{
		String assetAddress = GameConfig.ASSETS_GOLD;			
		goldPanel = new ImagedPanel(assetAddress, GRID_LENGTH, GRID_LENGTH);
		goldPanel.setOpaque(false);
		
		goldPanel.setBounds(getGridLeft(5), getGridTop(5), GRID_LENGTH, GRID_LENGTH);
		container.add(goldPanel);
		container.moveToFront(goldPanel);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#newDwarf(goldrun.game.DwarfController)
	 */
	public void newDwarf(DwarfController dwarf)
		throws IOException
	{
		String assetAddress = GameConfig.ASSETS_DWARFUP;	
		ImagedPanel dwarfPanel = new ImagedPanel(assetAddress, GRID_LENGTH, GRID_LENGTH);
		dwarfPanel.setOpaque(false);
		
		JLabel label = new JLabel("009");
		label.setForeground(Color.RED);
		label.setBounds(getGridLeft(0), getGridTop(0), GRID_LENGTH * 2, 20);
		label.setOpaque(false);
		
		JLabel goldlabel = new JLabel("009");
		goldlabel.setForeground(Color.ORANGE);
		goldlabel.setBackground(Color.ORANGE);
		goldlabel.setBounds(getGridLeft(0), getGridTop(0) - 20, GRID_LENGTH, 20);
		goldlabel.setOpaque(false);
		
		dwarfPanel.setBounds(getGridLeft(0), getGridTop(0), GRID_LENGTH, GRID_LENGTH);
		container.add(dwarfPanel);
		container.add(label);
		container.add(goldlabel);
		container.moveToFront(dwarfPanel);
		container.moveToFront(label);
		container.moveToFront(goldlabel);
		
		DwarfRepresent dwarfInstance = new DwarfRepresent(controller, dwarf, dwarfPanel, label, goldlabel, GameConfig.ASSETS_DWARFUP, GameConfig.ASSETS_DWARFDOWN,
			GameConfig.ASSETS_DWARFLEFT, GameConfig.ASSETS_DWARFRIGHT);
		sequencer.addInstance(dwarfInstance);
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#newGold(goldrun.game.Gold)
	 */
	public void newGold(Gold newGold)
	{
		goldPanel.setLocation(getGridLeft(newGold.getX()), getGridTop(newGold.getY()));
		container.moveToFront(goldPanel);
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#newBomb(goldrun.game.Bomb)
	 */
	public void newBomb(Bomb bomb)
		throws IOException
	{
		try
		{
			String assetAddress = GameConfig.ASSETS_BOMB;	
			ImagedPanel bombPanel = new ImagedPanel(assetAddress, GRID_LENGTH, GRID_LENGTH);
			bombPanel.setOpaque(false);
			
			bombPanel.setBounds(getGridLeft(bomb.getX()), getGridTop(bomb.getY()), GRID_LENGTH, GRID_LENGTH);
			container.add(bombPanel);
			container.moveToFront(bombPanel);
			
			JLabel label = new JLabel(bomb.getDwarf().getName());
			label.setBounds(getGridLeft(bomb.getX()), getGridTop(bomb.getY()) + GRID_LENGTH, GRID_LENGTH * 2, 20);
			label.setForeground(Color.LIGHT_GRAY);
			label.setOpaque(false);
			container.add(label);
			container.moveToFront(label);
			
			BombRepresent bombRepresent = new BombRepresent(bomb, bombPanel, label);
			bombs.add(bombRepresent);
		}
		catch (Exception ex)
		{
			Debug.out().println("X=" + getGridLeft(bomb.getX()) + ",Y=" + getGridTop(bomb.getY()));
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#detonateBomb(goldrun.game.Bomb)
	 */
	public void detonateBomb(Bomb bomb)
		throws IOException
	{
		String assetAddress = GameConfig.ASSETS_EXPLODED;	
		BombRepresent represent = getBombRepresent(bomb);
		if (represent != null)
		{
			represent.getRepresent().setImage(assetAddress);
			BombThread thread = new BombThread(this, bomb);
			thread.start();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#removeBomb(goldrun.game.Bomb)
	 */
	public void removeBomb(Bomb bomb)
	{
		BombRepresent represent = getBombRepresent(bomb);
		if (represent != null)
		{
			container.remove(represent.getRepresent());
			container.remove(represent.getLabel());
		}
		
		ImagedPanel panel = mazePanels[bomb.getX()][bomb.getY()];
		container.moveToFront(panel);
		container.moveToBack(panel);
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#repositionDwarf(goldrun.game.DwarfController, goldrun.game.Grid)
	 */
	public void repositionDwarf(DwarfController mouse, Grid grid)
	{
		DwarfRepresent thread = getDwarfInstance(mouse);
		if (thread != null)
		{
			ImagedPanel mousePanel = (ImagedPanel)thread.getRepresent();
			mousePanel.setBounds(getGridLeft(grid.getX()), getGridTop(grid.getY()), GRID_LENGTH, GRID_LENGTH);	
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#clearDwarf()
	 */
	public void clearDwarf()
	{
		for (DwarfRepresent mouseThread : sequencer.getInstances())
		{
			JPanel represent = mouseThread.getRepresent();
			container.remove(represent);
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#start()
	 */
	public void start()
	{
		sequencer.start();
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#stop()
         * Modified on 01-21-2016: New mouse variables shown in the end
	 */
	public void stop()
	{	
		sequencer.kill();
		countDownThread.kill();
		
		int highestNumberOfGold = -1;
		for (DwarfRepresent dwarfThread : sequencer.getInstances())
		{
			if (highestNumberOfGold == -1)
			{
				highestNumberOfGold = dwarfThread.getDwarfController().getNumberOfGold();
			}
			else
			{
				DwarfController controller = dwarfThread.getDwarfController();
				if (highestNumberOfGold < controller.getNumberOfGold())
				{
					highestNumberOfGold = controller.getNumberOfGold();	
				}
			}
		}
		
		ArrayList<DwarfController> winners = new ArrayList<DwarfController>();
		for (DwarfRepresent mouseThread : sequencer.getInstances())
		{
			DwarfController controller = mouseThread.getDwarfController();
			if (controller.getNumberOfGold() == highestNumberOfGold)
			{
				winners.add(controller);
			}
		}
		
		String newline = System.getProperty("line.separator");
		String message = (winners.size() == 1 ? "Ganador" : "Empate!") + newline;
		
		int index = 1;
		for (DwarfController controller : winners)
		{
			message += "(" + index + ") " + controller.getDwarf().getName() + newline;
			index++;
		}
		
		message += newline + newline + "Resultados:" + newline;
		index = 1;
		for (DwarfRepresent dwarfThread : sequencer.getInstances())
		{
			DwarfController controller = dwarfThread.getDwarfController();
                        // Modified: 01-21-2016
			message += "(" + index + ") " + controller.getDwarf().getName() + " -- " + controller.getNumberOfGold() + " tesoro(s)" 
                                    + ", " + controller.getDwarf().getSteps()+ " paso(s)" 
                                    + ", " + controller.getDwarf().getExploredGrids()+ " celda(s) visitada(s)" + newline;
			index++;
		}
		
		JOptionPane.showMessageDialog(null, message, "Resultados", JOptionPane.INFORMATION_MESSAGE);
		Debug.out().println();
		Debug.out().println();
		Debug.out().println(message);
		System.exit(0);
	}
	
	// Converts the Maze X value to the Left value of the Game Interface
	private int getGridLeft(int x)
	{
		return x * GRID_LENGTH;
	}
	
	// Converts the Maze Y value to the Top value of the Game Interface
	private int getGridTop(int y)
	{
		return (maze.getHeight() - y - 1) * GRID_LENGTH;
	}
	
	// Get the MouseInstance that represents the Dwarf and the DwarfController in the
	// game interface
	private DwarfRepresent getDwarfInstance(DwarfController dwarf)
	{
		for (DwarfRepresent dwarfThread : sequencer.getInstances())
		{
			if (dwarfThread.getDwarfController() == dwarf)
			{
				return dwarfThread;
			}
		}
		
		return null;
	}
	
	// Gets the BombRepresent that represents the bomb in the game interface.
	private BombRepresent getBombRepresent(Bomb bomb)
	{
		for (BombRepresent represent : bombs)
		{
			if (represent.getBomb() == bomb)
			{
				return represent;
			}
		}
		
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see goldrun.game.GameControllerAdapter#displayCountDown(int seconds)()
	 */
	public void displayCountDown(int seconds)
	{
		countDownLabel.setText(seconds + "");
		Dimension preferred = countDownLabel.getPreferredSize();
		
		int y = (int)((container.getHeight() - preferred.getHeight()) / 2);
		int x = (int)((container.getWidth() - preferred.getWidth()) / 2);
		
		countDownLabel.setBounds(x, y, (int)preferred.getWidth(), (int)preferred.getHeight());
		container.moveToFront(countDownLabel);
	}
	
}
