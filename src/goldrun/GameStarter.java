/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MouseRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun;
import goldrun.game.GameUI;
import goldrun.game.GameSecurityManager;

/**
 * Class GameStarter is the Main class of MouseRun.
 */
public class GameStarter
{
	
	public static void main(String... args)
	{
		try
		{
			int width = Integer.parseInt(args[0]);
			int height = Integer.parseInt(args[1]);
			int numberOfCheese = Integer.parseInt(args[2]);
			int duration = 0;
						
			if (width < 5 || height < 5 || numberOfCheese < 1)
			{
				printHelp();
				return;
			}
			
			if (args.length == 4)
			{
				duration = Integer.parseInt(args[3]);
				System.out.println("El juego durar\u00E1 " + duration + " segundos");
			}
		
			
			GameSecurityManager.install();
			GameUI ui = new GameUI(width, height, numberOfCheese, duration);
			ui.setVisible(true);
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			printHelp();
		}
	}
	
	private static void printHelp()
	{
		System.out.println();
		System.out.println();
		System.out.println("Gold Run Execution");
		System.out.println();
		System.out.println("java [classpath] goldrun.GameStarter <ancho> <alto> <oro> {duraci\u00F3n}");
		System.out.println();
		System.out.println("[classpath]\tSi es necesario, incluir la ruta del classpath. etc: -cp classes");
		System.out.println("<ancho>\t\tN\u00FAmero de columnas en el laberinto. Entero. OBLIGATORIO. Min: 5");
		System.out.println("<alto>\tN\u00FAmero de filas en el laberinto. Entero. OBLIGATORIO. Min: 5");
		System.out.println("<oro>\tN\u00FAmero de cofres de oro. Entero. OBLIGATORIO. Min: 1");
		System.out.println("{duraci\u00F3n}\tOpcional. La duraci\u00F3n, en segundos, de la partida.");
		System.out.println();
	}

}