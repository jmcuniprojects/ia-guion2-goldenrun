/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goldrun.dwarf;

import goldrun.game.Dwarf;
import goldrun.game.Gold;
import goldrun.game.Grid;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author admin
 */
public class D19A12a extends Dwarf {

    private class coordenadas {

        public int x;
        public int y;

        public coordenadas(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "coordenadas{" + "x=" + x + ", y=" + y + '}';
        }
    }

    private class pilaM {

        public ArrayList<Integer> movimientos;
        private int tam;
        private int ocupados;

        public pilaM(int t) {
            this.movimientos = new ArrayList<>();
            this.tam = t;
            this.ocupados = 0;
        }

        public void add(int a) {
            if (ocupados == tam) {
                movimientos.remove(tam - 1);
            }
            movimientos.add(0, a);
        }

        public int lastMove() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            return aux;
        }

        public int lastMoveInverse() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            switch (aux) {
                case 1:
                    return 2;
                case 2:
                    return 1;
                case 3:
                    return 4;
                case 4:
                    return 3;
                default:
                    return 5;
            }
        }

        public void clear() {
            movimientos.clear();
        }
    }
    private Hashtable<String, ArrayList<coordenadas>> hash;
    private Hashtable<String, ArrayList<coordenadas>> hashRecorridas;
    private Random generador;
    private pilaM lastMove;
    private int mov;

    public D19A12a() {
        super("ShieldHero");
        generador = new Random();
        hash = new Hashtable<>();
        hashRecorridas = new Hashtable<>();
        lastMove = new pilaM(5);
    }

    @Override
    public int move(Grid currentGrid, Gold gold) {
        ArrayList<coordenadas> posiblesCasillas = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            if (currentGrid.canGoUp()) {
                posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() + 1)));
            }
            if (currentGrid.canGoDown()) {
                posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() - 1)));
            }
            if (currentGrid.canGoLeft()) {
                posiblesCasillas.add(new coordenadas((currentGrid.getX() - 1), currentGrid.getY()));
            }
            if (currentGrid.canGoRight()) {
                posiblesCasillas.add(new coordenadas((currentGrid.getX() + 1), currentGrid.getY()));
            }
        }
        String aux = currentGrid.getX() + ";" + currentGrid.getY();
        hash.putIfAbsent(aux, posiblesCasillas);
        hashRecorridas.putIfAbsent(aux, posiblesCasillas);

        int i = 1;
        String sig = posiblesCasillas.get(0).x + ";" + posiblesCasillas.get(0).y;
        while (hash.containsKey(sig) && i < posiblesCasillas.size()) {
            sig = posiblesCasillas.get(i).x + ";" + posiblesCasillas.get(i).y;
            i++;
        }
        if (i != posiblesCasillas.size()) {
            Scanner scan = new Scanner(sig).useDelimiter(";");
            int xx = scan.nextInt();
            int yy = scan.nextInt();
            if (!hashRecorridas.containsKey(xx + ";" + yy)) {
                incExploredGrids();
            }
            if (yy > currentGrid.getY()) {
                mov = 1;
                lastMove.add(1);
            } else {
                if (yy < currentGrid.getY()) {
                    mov = 2;
                    lastMove.add(2);
                } else {
                    if (xx < currentGrid.getX()) {
                        mov = 3;
                        lastMove.add(3);
                    } else {
                        mov = 4;
                        lastMove.add(4);
                    }
                }
            }
        } else {
            if (!lastMove.movimientos.isEmpty()) {
                mov = lastMove.lastMoveInverse();
            } else {
                Random gen = new Random();
                int escape = gen.nextInt(posiblesCasillas.size() - 1);
                coordenadas obj = posiblesCasillas.get(escape);
                if (obj.y > currentGrid.getY()) {
                    mov = 1;
                } else {
                    if (obj.y < currentGrid.getY()) {
                        mov = 2;
                    } else {
                        if (obj.x < currentGrid.getX()) {
                            mov = 3;
                        } else {
                            mov = 4;
                        }
                    }
                }
            }
        }

        switch (mov) {
            case 1:
                return Dwarf.UP;
            case 2:
                return Dwarf.DOWN;
            case 3:
                return Dwarf.LEFT;
            case 4:
                return Dwarf.RIGHT;
            default:
                return Dwarf.BOMB;

        }
    }

    @Override
    public void newGold() {
    }

    @Override
    public void respawned() {
        lastMove.clear();
    }

}
