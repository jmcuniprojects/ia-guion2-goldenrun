/** 
 * GoldRun. A programming game to practice building intelligent things.
 * Based on MouseRun. Copyright (C) 2013  Muhammad Mustaqim
 * Recoded by Jose Maria Serrano - UJA 2019
 * 
 * This file is part of GoldRun.
 *
 * GoldRun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GoldRun is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GoldRun.  If not, see <http://www.gnu.org/licenses/>.
 **/
package goldrun.dwarf;

import goldrun.game.Dwarf;
import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Class DwarfLoader detects and load all Dwarf implementations
 packaged under dwarfrun.dwarf
 */
public class DwarfLoader
{
	private static final String PACKAGE = "goldrun.dwarf.";
	private static ArrayList<Class<?>> detected = null;

	/**
	 * Detects all Dwarf Implementations in the package goldrun.dwarf.
	 * @return All the Dwarf Implementation Classes which can be used to seed an instance.
	 */
	public static ArrayList<Class<?>> load()
	{
		detected = new ArrayList<Class<?>>();
		
		try
		{
			File directory = new File(getClassDirectory());
			File[] classFiles = directory.listFiles();
			
			if (classFiles != null)
			{
				for (File file : classFiles)
				{
					if (file.getName().endsWith(".class"))
					{
						String className = PACKAGE + file.getName().replace(".class", "");
						
						Class<?> clz = Class.forName(className);
					
						if (clz.getSuperclass() == Dwarf.class)
						{						
							//System.out.println("Dwarf Detected: " + clz.getSimpleName());
							detected.add(clz);
						}
					}
				}
			}
			//System.out.println();
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		return detected;
	}
	
	public static ArrayList<Class<?>> getDetectedDwarfClasses()
	{
		if (detected == null)
		{
			detected = new ArrayList<Class<?>>();
		}
		
		return detected;
	}

	// Gets the current directory of the DwarfLoader that will be used as an anchor point
	// to get all other Dwarf in the same package. This method will throw IllegalStateException
	// if the DwarfLoader is not a directory, because this game is only designed to be
	// executed using the java command through the GameStarter.class 
	private static String getClassDirectory()
	{
		String file = "DwarfLoader.class";
		URL location = DwarfLoader.class.getResource(file);
		
		if (!location.getProtocol().equalsIgnoreCase("file"))
		{
			throw new IllegalStateException("GoldRun is not intended to run in this manner.");
		}
		
		String locationPath = location.getPath();
		locationPath = locationPath.substring(0, locationPath.length() - file.length());
		
		try
		{
			locationPath = URLDecoder.decode(locationPath, "UTF-8");
		}
		catch (UnsupportedEncodingException uee)
		{
			// do nothing here.
		}
		
		return locationPath;
	}
	
}
