/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goldrun.dwarf;

import goldrun.game.Dwarf;
import goldrun.game.Gold;
import goldrun.game.Grid;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import jdk.nashorn.internal.ir.BreakNode;

/**
 *
 * @author admin
 */
public class D19A12b extends Dwarf {

    private class ArrayListCoor extends ArrayList<Object> {

        @Override
        public boolean contains(Object o) {
            for (Object p : toArray()) {
                if (p == o) {
                    return true;
                }
            }
            return false;
        }

    }

    private class coordenadas {

        public int x;
        public int y;

        public coordenadas(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public coordenadas(coordenadas orig) {
            this.x = orig.x;
            this.y = orig.y;
        }

        @Override
        public String toString() {
            return x + ";" + y;
        }

        public boolean equals(coordenadas obj) {
            if (x == obj.x) {
                if (y == obj.y) {
                    return true;
                }
            }
            return false;
        }

    }

    private class pilaM {

        public ArrayList<Integer> movimientos;
        private int tam;
        private int ocupados;

        public pilaM(int t) {
            this.movimientos = new ArrayList<>();
            this.tam = t;
            this.ocupados = 0;
        }

        public void add(int a) {
            if (ocupados == tam) {
                movimientos.remove(tam - 1);
            }
            movimientos.add(0, a);
        }

        public int lastMove() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            return aux;
        }

        public int lastMoveInverse() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            switch (aux) {
                case 1:
                    return 2;
                case 2:
                    return 1;
                case 3:
                    return 4;
                case 4:
                    return 3;
                default:
                    return 5;
            }
        }

        public void clear() {
            movimientos.clear();
        }
    }

    private Hashtable<String, ArrayList<coordenadas>> hash;
    private Random generador;
    private pilaM lastMove;
    private boolean busqueda;
    private int mov;
    private ArrayDeque<Integer> listaMov;

    public D19A12b() {
        super("Filo");
        generador = new Random();
        hash = new Hashtable<>();
        lastMove = new pilaM(20);
        busqueda = false;
        listaMov = new ArrayDeque<>();
    }

    private ArrayList<coordenadas> checkRound(Grid currentGrid) {
        ArrayList<coordenadas> posiblesCasillas = new ArrayList<>();
        if (currentGrid.canGoUp()) {
            posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() + 1)));
        }
        if (currentGrid.canGoDown()) {
            posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() - 1)));
        }
        if (currentGrid.canGoLeft()) {
            posiblesCasillas.add(new coordenadas((currentGrid.getX() - 1), currentGrid.getY()));
        }
        if (currentGrid.canGoRight()) {
            posiblesCasillas.add(new coordenadas((currentGrid.getX() + 1), currentGrid.getY()));
        }
        return posiblesCasillas;
    }

    private int checkRoundIs(coordenadas origen, coordenadas objetivo) {
        int aux = -1;
        if (objetivo.y > origen.y) {
            aux = 1;
        } else {
            if (objetivo.y < origen.y) {
                aux = 2;
            } else {
                if (objetivo.x < origen.x) {
                    aux = 3;
                } else {
                    aux = 4;
                }
            }
        }
        return aux;
    }

    private int countRound(ArrayList<coordenadas> posiblesCasillas) {
        int i = 0;
        String sig = posiblesCasillas.get(0).toString();
        for (int j = 0; j < posiblesCasillas.size(); j++) {
            if (!hash.containsKey(sig)) {
                i++;
            }
            sig = posiblesCasillas.get(j).x + ";" + posiblesCasillas.get(j).y;
        }
        return i;
    }

    private void exploracion(Grid currentGrid, ArrayList<coordenadas> posiblesCasillas, coordenadas oro) {
        int i = 0;
        boolean tesoroCerca = false;
        String sig = posiblesCasillas.get(0).toString();
        while (hash.containsKey(sig) && i < posiblesCasillas.size()) {
            tesoroCerca = posiblesCasillas.get(i).equals(oro);
            if (!tesoroCerca) {
                i++;
                if (i < posiblesCasillas.size()) {
                    sig = posiblesCasillas.get(i).x + ";" + posiblesCasillas.get(i).y;
                }
            }
        }
        if (i != posiblesCasillas.size()) {
            Scanner scan = new Scanner(sig).useDelimiter(";");
            int xx = scan.nextInt();
            int yy = scan.nextInt();
            if (!hash.containsKey(xx + ";" + yy)) {
                incExploredGrids();
            }
            coordenadas org = new coordenadas(currentGrid.getX(), currentGrid.getY());
            coordenadas obj = new coordenadas(xx, yy);
            mov = checkRoundIs(org, obj);
            lastMove.add(mov);
        } else {
            if (!lastMove.movimientos.isEmpty()) {
                mov = lastMove.lastMoveInverse();
            } else {
                Random gen = new Random();
                int escape = gen.nextInt(posiblesCasillas.size()-1);
                coordenadas org = new coordenadas(currentGrid.getX(), currentGrid.getY());
                coordenadas obj = posiblesCasillas.get(escape);
                mov = checkRoundIs(org, obj);
            }
        }
    }

    private boolean bProfundidad(coordenadas paso, ArrayDeque<coordenadas> movList, coordenadas objetivo, Hashtable<String, Integer> visitadas) {
        String pasoS = paso.toString();
        visitadas.putIfAbsent(pasoS, 1);
        if (!paso.equals(objetivo)) {
            if (hash.get(pasoS) != null) {
                for (coordenadas pasoSig : hash.get(pasoS)) {
                    if (!visitadas.containsKey(pasoSig.toString())) {
                        boolean caminoOk = bProfundidad(pasoSig, movList, objetivo, visitadas);
                        if (caminoOk) {
                            movList.push(paso);
                            return true;
                        }
                    }
                }
            }
        } else {
            movList.add(paso);
            return true;
        }
        return false;
    }

    private boolean buscaCaminoPP(ArrayDeque<coordenadas> profundidadArbol, Grid currentGrid, Gold gold) {
        coordenadas oro = new coordenadas(gold.getX(), gold.getY());
        boolean caminoOk = false;
        ArrayList<coordenadas> listaAux = hash.get(currentGrid.getX() + ";" + currentGrid.getY());
        Hashtable<String, Integer> listaCasillasVisit = new Hashtable<>();

        int cont = 0;
        while (!caminoOk && cont < listaAux.size()) {
            caminoOk = bProfundidad((coordenadas) listaAux.get(cont), profundidadArbol, oro, listaCasillasVisit);
            listaCasillasVisit.clear();
            cont++;
        }
        if (!listaCasillasVisit.isEmpty()) {
            listaCasillasVisit.clear();
        }
        if (profundidadArbol.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private void generaListaMov(ArrayDeque<coordenadas> profundidadArbol, Grid currentGrid) {
        coordenadas pasoAux = new coordenadas(currentGrid.getX(), currentGrid.getY());
        while (!profundidadArbol.isEmpty()) {
            coordenadas pasoOr = pasoAux;
            coordenadas pasoDe = profundidadArbol.poll();
            listaMov.add(checkRoundIs(pasoOr, pasoDe));

            pasoAux = pasoDe;
        }
    }

    @Override
    public int move(Grid currentGrid, Gold gold) {
        ArrayList<coordenadas> posiblesCasillas = checkRound(currentGrid);
        String aux = currentGrid.getX() + ";" + currentGrid.getY();
        hash.putIfAbsent(aux, posiblesCasillas);
        coordenadas oro = new coordenadas(gold.getX(), gold.getY());
        if (hash.containsKey(gold.getX() + ";" + gold.getY())) {
            busqueda = true;
        }
        if (countRound(posiblesCasillas) != posiblesCasillas.size()) {
            if (!busqueda) {
                exploracion(currentGrid, posiblesCasillas, oro);
            } else {
                if (listaMov.isEmpty()) {
                    ArrayDeque<coordenadas> profundidadArbol = new ArrayDeque<>();

                    boolean caminoOk = buscaCaminoPP(profundidadArbol, currentGrid, gold);
                    if (caminoOk) {
                        generaListaMov(profundidadArbol, currentGrid);
                        mov = listaMov.poll();
                        lastMove.add(mov);
                    } // Si pisamos una bomba puede ser que no haya camino posible hasta el tesoro
                    else {
                        busqueda = false;
                        listaMov.clear();
                        exploracion(currentGrid, posiblesCasillas, oro);
                    }
                } else {
                    mov = listaMov.poll();
                    lastMove.add(mov);
                }
            }
        } else {
            exploracion(currentGrid, posiblesCasillas, oro);
        }
        switch (mov) {
            case 1:
                return Dwarf.UP;
            case 2:
                return Dwarf.DOWN;
            case 3:
                return Dwarf.LEFT;
            case 4:
                return Dwarf.RIGHT;
            default:
                return Dwarf.BOMB;

        }
    }

    @Override
    public void newGold() {
        busqueda = false;
        listaMov.clear();
    }

    @Override
    public void respawned() {
        lastMove.clear();
    }

}
