/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goldrun.dwarf;

import goldrun.game.Dwarf;
import goldrun.game.Gold;
import goldrun.game.Grid;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;
import javafx.util.Pair;

/**
 *
 * @author admin
 */
public class D19A12c extends Dwarf {

    private class coordenadas {

        public int x;
        public int y;

        public coordenadas(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public coordenadas(coordenadas orig) {
            this.x = orig.x;
            this.y = orig.y;
        }

        @Override
        public String toString() {
            return x + ";" + y;
        }

        public boolean equals(coordenadas obj) {
            if (x == obj.x) {
                if (y == obj.y) {
                    return true;
                }
            }
            return false;
        }

    }

    private class pilaM {

        public ArrayList<Integer> movimientos;
        private int tam;
        private int ocupados;

        public pilaM(int t) {
            this.movimientos = new ArrayList<>();
            this.tam = t;
            this.ocupados = 0;
        }

        public void add(int a) {
            if (ocupados == tam) {
                movimientos.remove(tam - 1);
            }
            movimientos.add(0, a);
        }

        public int lastMove() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            return aux;
        }

        public int lastMoveInverse() {
            int aux = movimientos.get(0);
            movimientos.remove(0);
            switch (aux) {
                case 1:
                    return 2;
                case 2:
                    return 1;
                case 3:
                    return 4;
                case 4:
                    return 3;
                default:
                    return 5;
            }
        }

        public void clear() {
            movimientos.clear();
        }
    }

    private Hashtable<String, ArrayList<coordenadas>> hash;
    private pilaM lastMove;
    private boolean busqueda;
    private int mov;
    private ArrayDeque<Integer> listaMov;
    private coordenadas origen;

    public D19A12c() {
        super("Raphtalia");
        hash = new Hashtable<>();
        lastMove = new pilaM(10000);
        busqueda = false;
        listaMov = new ArrayDeque<>();
    }

    private ArrayList<coordenadas> checkRound(Grid currentGrid) {
        ArrayList<coordenadas> posiblesCasillas = new ArrayList<>();
        if (currentGrid.canGoUp()) {
            posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() + 1)));
        }
        if (currentGrid.canGoDown()) {
            posiblesCasillas.add(new coordenadas(currentGrid.getX(), (currentGrid.getY() - 1)));
        }
        if (currentGrid.canGoLeft()) {
            posiblesCasillas.add(new coordenadas((currentGrid.getX() - 1), currentGrid.getY()));
        }
        if (currentGrid.canGoRight()) {
            posiblesCasillas.add(new coordenadas((currentGrid.getX() + 1), currentGrid.getY()));
        }
        return posiblesCasillas;
    }

    private int checkRoundIs(coordenadas origen, coordenadas objetivo) {
        int aux = -1;
        if (objetivo.y > origen.y) {
            aux = 1;
        } else {
            if (objetivo.y < origen.y) {
                aux = 2;
            } else {
                if (objetivo.x < origen.x) {
                    aux = 3;
                } else {
                    aux = 4;
                }
            }
        }
        return aux;
    }

    private int countRound(ArrayList<coordenadas> posiblesCasillas) {
        int i = 0;
        String sig = posiblesCasillas.get(0).toString();
        for (int j = 0; j < posiblesCasillas.size(); j++) {
            if (!hash.containsKey(sig)) {
                i++;
            }
            sig = posiblesCasillas.get(j).x + ";" + posiblesCasillas.get(j).y;
        }
        return i;
    }

    private void exploracion(Grid currentGrid, ArrayList<coordenadas> posiblesCasillas, coordenadas oro) {
        int i = 0;
        boolean tesoroCerca = false;
        String sig = posiblesCasillas.get(0).toString();
        while (hash.containsKey(sig) && i < posiblesCasillas.size()) {
            tesoroCerca = posiblesCasillas.get(i).equals(oro);
            if (!tesoroCerca) {
                i++;
                if (i < posiblesCasillas.size()) {
                    sig = posiblesCasillas.get(i).x + ";" + posiblesCasillas.get(i).y;
                }
            }
        }
        if (i != posiblesCasillas.size()) {
            Scanner scan = new Scanner(sig).useDelimiter(";");
            int xx = scan.nextInt();
            int yy = scan.nextInt();
            if (!hash.containsKey(xx + ";" + yy)) {
                incExploredGrids();
            }
            coordenadas org = new coordenadas(currentGrid.getX(), currentGrid.getY());
            coordenadas obj = new coordenadas(xx, yy);
            mov = checkRoundIs(org, obj);
            lastMove.add(mov);
        } else {
            if (!lastMove.movimientos.isEmpty()) {
                mov = lastMove.lastMoveInverse();
            }  else {
                Random gen = new Random();
                int escape = gen.nextInt(posiblesCasillas.size()-1);
                coordenadas org = new coordenadas(currentGrid.getX(), currentGrid.getY());
                coordenadas obj = posiblesCasillas.get(escape);
                mov = checkRoundIs(org, obj);
            }
        }
    }

    private boolean creaCamino(coordenadas actual, coordenadas objetivo, ArrayDeque<Integer> camino) {
        try {
            if (!actual.equals(objetivo)) {
                Hashtable<String, Pair<coordenadas, coordenadas>> caminoExp = new Hashtable<>();

                estrella(actual, objetivo, caminoExp);

                Pair<coordenadas, coordenadas> temp = caminoExp.get(objetivo.toString());
                camino.push(checkRoundIs(temp.getValue(), temp.getKey()));

                caminoExp.remove(objetivo.toString());

                while (!temp.getValue().equals(actual)) {
                    temp = caminoExp.get(temp.getValue().toString());
                    int aux = checkRoundIs(temp.getValue(), temp.getKey());
                    camino.push(aux);
                }
                if (!camino.isEmpty()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } catch (NullPointerException ex) {
            return false;
        }
    }

    private void estrella(coordenadas actual, coordenadas objetivo, Hashtable<String, Pair<coordenadas, coordenadas>> camino) {
        ArrayList<Pair<Integer, coordenadas>> posibilidades = new ArrayList<>();//< Todas las posibilidades
        ArrayList<Pair<Integer, coordenadas>> posibilidadesMas = new ArrayList<>(); //< Las posibilidades dentro de la casilla mas prometedora
        Hashtable<String, Pair<Integer, coordenadas>> cerrados = new Hashtable<>();
        boolean encontrado = false;

        Pair<Integer, coordenadas> par = new Pair(0, actual);
        posibilidades.add(par);
        posibilidadesMas.add(par);

        while (!posibilidades.isEmpty() && !encontrado) {
            int dMin = 1000000;

            boolean prometedor = true;
            int i = 0;
            int mejor = 0;
            Pair<Integer, coordenadas> auxiliar2 = new Pair(0, actual);

            if (posibilidadesMas.isEmpty()) {
                prometedor = false;
                while (i < posibilidades.size() && !encontrado) {
                    Pair<Integer, coordenadas> auxiliar = posibilidades.get(i);
                    if (auxiliar.getValue().equals(objetivo)) {
                        mejor = i;
                        encontrado = true;
                        break;
                    }

                    int costo = manhattanA(objetivo, auxiliar.getValue());
                    if (costo < dMin) {
                        dMin = costo;
                        mejor = i;
                    }
                    i++;
                }
            } else {
                while (!posibilidadesMas.isEmpty()) {
                    Pair<Integer, coordenadas> auxiliar = posibilidadesMas.remove(0);
                    if (auxiliar.getValue().equals(objetivo)) {
                        mejor = i;
                        encontrado = true;
                        break;
                    }

                    int costo = manhattanA(objetivo, auxiliar.getValue());
                    if (costo < dMin) {
                        dMin = costo;
                        mejor = i;
                        auxiliar2 = auxiliar;
                    }
                }
            }
            Pair<Integer, coordenadas> temp;
            if (prometedor) {
                temp = auxiliar2;
                posibilidades.remove(temp);
            } else {
                temp = posibilidades.get(mejor);
                posibilidades.remove(mejor);
            }
            cerrados.put(temp.getValue().toString(), temp);
            int nivel = 1 + temp.getKey();

            if (hash.containsKey(temp.getValue().toString())) {
                ArrayList<coordenadas> hijos = hash.get(temp.getValue().toString());
                for (int j = 0; j < hijos.size(); j++) {
                    if (!cerrados.containsKey(hijos.get(j).toString())) {
                        posibilidades.add(new Pair(nivel, hijos.get(j)));
                        posibilidadesMas.add(new Pair(nivel, hijos.get(j)));
                        camino.putIfAbsent(hijos.get(j).toString(), new Pair(hijos.get(j), temp.getValue()));
                    }
                }
            }
        }
    }

    private int manhattanA(coordenadas tesoro, coordenadas current) {
        return ((Math.abs(origen.x - current.x)) + (Math.abs(origen.y - current.y)) + (Math.abs(current.x - tesoro.x)) + (Math.abs(current.y - tesoro.y)));
    }

    @Override
    public int move(Grid currentGrid, Gold gold) {
        ArrayList<coordenadas> posiblesCasillas = checkRound(currentGrid);
        String aux = currentGrid.getX() + ";" + currentGrid.getY();
        hash.putIfAbsent(aux, posiblesCasillas);
        coordenadas oro = new coordenadas(gold.getX(), gold.getY());
        if (hash.containsKey(gold.getX() + ";" + gold.getY())) {
            busqueda = true;
        }
        if (countRound(posiblesCasillas) != posiblesCasillas.size()) {
            if (!busqueda) {
                exploracion(currentGrid, posiblesCasillas, oro);
            } else {
                if (listaMov.isEmpty()) {
                    ArrayDeque<Integer> profundidadArbol = new ArrayDeque<>();
                    origen = new coordenadas(currentGrid.getX(), currentGrid.getY());
                    boolean caminoOk = creaCamino(origen, oro, profundidadArbol);
                    if (caminoOk) {
                        listaMov = profundidadArbol;
                        mov = listaMov.poll();
                        lastMove.add(mov);
                    }
                    else {
                        busqueda = false;
                        listaMov.clear();
                        exploracion(currentGrid, posiblesCasillas, oro);
                    }
                } else {
                    mov = listaMov.poll();
                    lastMove.add(mov);
                }
            }
        } else {
            exploracion(currentGrid, posiblesCasillas, oro);
        }
        switch (mov) {
            case 1:
                return Dwarf.UP;
            case 2:
                return Dwarf.DOWN;
            case 3:
                return Dwarf.LEFT;
            case 4:
                return Dwarf.RIGHT;
            default:
                return Dwarf.BOMB;

        }
    }

    @Override
    public void newGold() {
        listaMov.clear();
    }

    @Override
    public void respawned() {
        lastMove.clear();
        listaMov.clear();
    }

}
